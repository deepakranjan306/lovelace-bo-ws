module.exports = {

  development: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'root',
      password: 'root123',
      database: 'lovalacedb',
    },
    migrations:{
      directory: __dirname + '/database/migrations'
    },
    seeds:{
      directory: __dirname + '/database/seeds'
    },
  }
};

