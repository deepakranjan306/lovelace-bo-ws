
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('clients_client').del()
    .then(function () {
      // Inserts seed entries
      return knex('clients_client').insert([
        {id: 1, name: 'John Bell', doj: '2018-01-02', expiry: 10, tenant_id: 1},
        {id: 2, name: 'Bell Smith', doj: '2018-01-13', expiry: 15, tenant_id: 1},
        {id: 3, name: 'Anoop MD', doj: '2018-03-05', expiry: 20, tenant_id: 1},
        {id: 4, name: 'Sunil G', doj: '2018-04-23', expiry: 25, tenant_id: 1},
        {id: 5, name: 'Akash', doj: '2018-05-15', expiry: 28, tenant_id: 1}
      ]);
    });
};
