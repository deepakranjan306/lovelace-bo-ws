
exports.up = function(knex, Promise) {
	return knex.schema.createTableIfNotExists('clients_client', function(table){
		table.increments('id').unsigned().primary();
		table.string('name').notNull();
		table.date('doj').notNull();
		table.integer('expiry').notNull();
		table.integer('tenant_id').unsigned().notNull();
		table.timestamp('created_at').defaultTo(knex.fn.now());
		table.timestamp('updated_at').defaultTo(knex.fn.now());
	})
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('clients_client');
};
