'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ClientDao = require('../dao/client');
var Joi = require('joi');
var clientSchema = require('../schema/client');
var idSchema = require('../schema/id');

var Client = function () {
	function Client() {
		_classCallCheck(this, Client);

		this.clientDao = new ClientDao();
	}

	_createClass(Client, [{
		key: 'validate',
		value: function validate(data) {
			return new Promise(function (resolve, reject) {
				var _Joi$validate = Joi.validate(data, clientSchema, { abortEarly: false }),
				    error = _Joi$validate.error,
				    value = _Joi$validate.value;

				var validedValue = value;

				if (error) {
					error.badRequest = true;
					return reject(error);
				}
				return resolve(validedValue);
			});
		}
	}, {
		key: 'getAll',
		value: async function getAll() {
			try {
				var clients = await this.clientDao.getAll();
				return clients;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'getClientInfo',
		value: async function getClientInfo(clientId) {
			try {
				var client = await this.clientDao.getClientInfo(clientId);
				return client;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'getMeta',
		value: async function getMeta(tenantId) {
			try {
				var clients = await this.clientDao.getMeta(tenantId);
				return clients;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'create',
		value: async function create(client) {
			try {
				var validatedClient = await this.validate(client);
				var createdClient = await this.clientDao.create(validatedClient);
				return createdClient;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'update',
		value: async function update(client) {
			try {
				var result = Joi.validate(client.id, idSchema);
				if (result.error) {
					result.error.badRequest = true;
					throw result.error;
				}

				var validatedClient = await this.validate(client);
				var updatedClient = await this.clientDao.update(validatedClient);
				return updatedClient;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'remove',
		value: async function remove(clientId) {
			try {
				var result = Joi.validate(clientId, idSchema);
				if (result.error) {
					result.error.badRequest = true;
					throw result.error;
				}
				await this.clientDao.remove(clientId);
				return;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}]);

	return Client;
}();

module.exports = Client;