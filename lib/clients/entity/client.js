'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var knex = require('../../../database/knex');
var _ = require('lodash');
// var lovelaceKnex = require('lovelace-lib-knex');

var Client = function () {
	function Client() {
		_classCallCheck(this, Client);

		this.model = {};
	}

	_createClass(Client, [{
		key: 'setName',
		value: function setName(name) {
			this.model.name = name;
			return this;
		}
	}, {
		key: 'setDoj',
		value: function setDoj(doj) {
			this.model.doj = doj;
			return this;
		}
	}, {
		key: 'setExpiry',
		value: function setExpiry(expiry) {
			this.model.expiry = expiry;
			return this;
		}
	}, {
		key: 'setTenantId',
		value: function setTenantId(tenantId) {
			this.model.tenant_id = tenantId;
			return this;
		}
	}, {
		key: 'getDisplayable',
		value: function getDisplayable() {
			return ['id', 'name', 'doj', 'expiry'];
		}
	}, {
		key: 'getFillable',
		value: function getFillable() {
			return ['name', 'doj', 'expiry', 'tenant_id'];
		}
	}, {
		key: 'all',
		value: async function all() {
			var fields = this.getDisplayable();
			return knex.select.apply(knex, _toConsumableArray(fields)).from('clients_client');
		}
	}, {
		key: 'info',
		value: async function info(clientId) {
			var fields = this.getDisplayable();
			return knex.select.apply(knex, _toConsumableArray(fields)).from('clients_client').where('id', clientId);
		}
	}, {
		key: 'getMeta',
		value: async function getMeta(tenantId) {

			return knex.select('id', 'name').from('clients_client').where('tenant_id', tenantId);
		}
	}, {
		key: 'findById',
		value: async function findById(id) {
			try {
				var fields = this.getDisplayable();
				var client = await knex.select.apply(knex, _toConsumableArray(fields)).from('clients_client').where('id', id);
				return client;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'create',
		value: function create() {
			var columns = this.getFillable();
			return knex('clients_client').insert(_.pick(this.model, columns));
		}
	}, {
		key: 'update',
		value: function update(id) {
			var columns = ['id', 'name', 'doj', 'expiry'];
			return knex('clients_client').where('id', id).update(_.pick(this.model, columns));
		}
	}, {
		key: 'remove',
		value: function remove(id) {
			return knex('clients_client').where('id', id).del();
		}
	}]);

	return Client;
}();

module.exports = Client;