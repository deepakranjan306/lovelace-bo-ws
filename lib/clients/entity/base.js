'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LovelaceKnex = require('lovelace-lib-knex');
var config = require('config');

var BaseEntity = function BaseEntity() {
	_classCallCheck(this, BaseEntity);

	this.lovelaceKnex = new LovelaceKnex(config.database);
};

module.exports = BaseEntity;