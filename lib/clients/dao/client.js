'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ClientEntity = require('../entity/client');

var Client = function () {
	function Client() {
		_classCallCheck(this, Client);
	}

	_createClass(Client, [{
		key: 'getAll',
		value: async function getAll() {
			try {
				var clientEntity = new ClientEntity();
				var client = await clientEntity.all();
				return client;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'getClientInfo',
		value: async function getClientInfo(clientId) {
			try {
				var clientEntity = new ClientEntity();
				var client = await clientEntity.info(clientId);
				return client;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'getMeta',
		value: async function getMeta(tenantId) {
			try {
				var clientEntity = new ClientEntity();
				var clients = await clientEntity.getMeta(tenantId);
				return clients;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'create',
		value: async function create(client) {
			try {
				var clientEntity = new ClientEntity();

				clientEntity.setName(client.name).setDoj(client.doj).setExpiry(client.expiry).setTenantId(client.tenant_id);

				var createdClient = await clientEntity.create();

				var clientId = createdClient[0];
				return await clientEntity.findById(clientId);
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'update',
		value: async function update(client) {
			try {
				var clientEntity = new ClientEntity();

				clientEntity.setName(client.name).setDoj(client.doj).setExpiry(client.expiry);

				await clientEntity.update(client.id, client.tenant_id);

				return await clientEntity.findById(client.id);
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}, {
		key: 'remove',
		value: async function remove(clientId) {
			try {
				var clientEntity = new ClientEntity();

				await clientEntity.findById(clientId);
				await clientEntity.remove(clientId);
				return;
			} catch (error) {
				return Promise.reject(error);
			}
		}
	}]);

	return Client;
}();

module.exports = Client;