'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ClientManager = require('../manager/client');
var _ = require('lodash');

var ClientController = function () {
	function ClientController() {
		_classCallCheck(this, ClientController);
	}

	_createClass(ClientController, null, [{
		key: 'getAll',
		value: function getAll(req, res) {
			var clientManager = new ClientManager();

			clientManager.getAll().then(function (clients) {
				return res.send(clients);
			}).catch(function (error) {
				return res.send(error);
			});
		}
	}, {
		key: 'getMeta',
		value: function getMeta(req, res) {
			var clientManager = new ClientManager();

			clientManager.getMeta(1).then(function (client) {
				return res.send(client);
			}).catch(function (error) {
				return res.send(error);
			});
		}
	}, {
		key: 'getClientInfo',
		value: function getClientInfo(req, res) {
			var clientManager = new ClientManager();
			var clientId = Number(req.params.id);

			clientManager.getClientInfo(clientId).then(function (client) {
				return res.send(client);
			}).catch(function (error) {
				return res.send(error);
			});
		}
	}, {
		key: 'create',
		value: function create(req, res) {
			var keys = ['name', 'doj', 'expiry'];

			var data = _.extend({}, _.pick(req.body, keys), {
				tenant_id: 1
			});

			var clientManager = new ClientManager();

			clientManager.create(data).then(function (client) {
				return res.send(client);
			}).catch(function (error) {
				return res.send(error);
			});
		}
	}, {
		key: 'update',
		value: function update(req, res) {
			var keys = ['name', 'doj', 'expiry'];

			var data = _.extend({}, _.pick(req.body, keys), {
				id: Number(req.params.id)
			});

			var clientManager = new ClientManager();

			clientManager.update(data).then(function (client) {
				return res.send(client);
			}).catch(function (error) {
				return res.send(error);
			});
		}
	}, {
		key: 'remove',
		value: function remove(req, res) {
			var clientManager = new ClientManager();
			var clientId = Number(req.params.id);

			clientManager.remove(clientId).then(function () {
				return res.send("Deleted Id:" + req.params.id);
			}).catch(function (error) {
				return res.send(error);
			});
		}
	}]);

	return ClientController;
}();

module.exports = ClientController;