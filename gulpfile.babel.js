'use strict';

import gulp from 'gulp';
import path from 'path';
import del from 'del';
var $ = require('gulp-load-plugins')();

const SRC = 'src';
const DEST = 'lib';

const PATH = {
  src: path.join(SRC, '**/*.js')
};

gulp.task('default', ['watch']);
gulp.task('start', ['watch', 'nodemon']);

gulp.task('nodemon', function () {
  $.nodemon({
    script: 'src/index.js',
    ext: 'js html',
    watch: 'src',
    env: { 'NODE_ENV': 'development' }
  });
});

gulp.task('build', ['babel']);

gulp.task('watch', ['build'], function () {
  gulp.watch(PATH.src, ['babel:app']);
});

var onError = function(err){
  $.util.beep();
};

const lintTask = (src) =>
  () =>
    gulp.src(src)
    .pipe($.plumber({
      errorHandler: onError
    }))
    .pipe($.eslint({
      fix: true
    }))
    .pipe($.eslint.format())
    .pipe($.eslint.failAfterError());

gulp.task('lint:app', lintTask(PATH.src));

const babelTask = (obj) =>
  () =>
    gulp.src(obj.src)
    .pipe($.changed(obj.dest))
    .pipe($.babel())
    .pipe(gulp.dest(obj.dest));

gulp.task('babel', ['babel:app']);

gulp.task('babel:app', ['lint:app'], babelTask({
    src: PATH.src,
    dest: DEST
  }));

gulp.task('clean', ['clean:app']);
gulp.task('clean:app', () => del.sync([DEST]));