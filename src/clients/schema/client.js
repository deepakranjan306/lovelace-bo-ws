var Joi = require('joi');

var client = Joi.object().keys({
	id: Joi.number().integer().min(1).max(100000).optional(),
	name: Joi.string().min(1).max(200),
	doj: Joi.date(),
	expiry: Joi.number().integer().min(1).max(50),
	tenant_id: Joi.number().integer().min(1).max(100)
}).requiredKeys('name');

module.exports = client;

