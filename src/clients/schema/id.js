'use strict';

var Joi = require('joi');

var id = Joi.number().integer().min(1).max(100000).required();

module.exports = id;