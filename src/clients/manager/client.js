'use strict';

var ClientDao = require('../dao/client');
var Joi = require('joi');
var clientSchema = require('../schema/client');
var idSchema = require('../schema/id');

class Client {

	constructor() {
		this.clientDao = new ClientDao();
	}

	validate(data) {
		return new Promise((resolve, reject ) => {
			let {error, value} = Joi.validate(data, clientSchema, {abortEarly: false});
			let validedValue = value;

			if(error) {
				error.badRequest = true;
				return reject(error);
			}
			return resolve(validedValue);
		});
	}

	async getAll() {
		try {
			let clients = await this.clientDao.getAll();
			return clients;
		} catch(error) {
			return Promise.reject(error);
		}
	}

	async getClientInfo(clientId) {
		try {
			let client = await this.clientDao.getClientInfo(clientId);
			return client;			
		} catch(error) {
			return Promise.reject(error);
		}
	}

	async getMeta(tenantId) {
		try {
			let clients = await this.clientDao.getMeta(tenantId);
			return clients;
		} catch(error) {
			return Promise.reject(error);
		}
		
	}

	async create(client) {
		try {
			let validatedClient = await this.validate(client);
			let createdClient = await this.clientDao.create(validatedClient);
			return createdClient;
		} catch(error) {
			return Promise.reject(error);
		}
	}

	async update(client) {
		try {
			let result = Joi.validate(client.id, idSchema);
			if(result.error) {
				result.error.badRequest = true;
				throw result.error;
			}

			let validatedClient = await this.validate(client);
			let updatedClient = await this.clientDao.update(validatedClient);
			return updatedClient;
		} catch (error) {
			return Promise.reject(error);
		}
	}

	async remove(clientId) {
		try {
			let result = Joi.validate(clientId, idSchema);
			if(result.error) {
				result.error.badRequest = true;
				throw result.error;
			}
			await this.clientDao.remove(clientId);
			return;
		} catch(error) {
			return Promise.reject(error);
		}
	}
}

module.exports = Client;