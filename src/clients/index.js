'use strict';

var ClientController = require('./controller/client');
var express = require('express');
var router = express.Router();

router.get('/api/clients/client', ClientController.getAll);
router.get('/api/clients/client/info/:id', ClientController.getClientInfo);
router.get('/api/clients/client/meta', ClientController.getMeta);
router.post('/api/clients/client', ClientController.create);
router.put('/api/clients/client/:id', ClientController.update);
router.delete('/api/clients/client/:id', ClientController.remove);

module.exports = router;
