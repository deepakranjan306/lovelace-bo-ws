'use strict';

var knex = require('../../../database/knex');
var _ = require('lodash');
// var lovelaceKnex = require('lovelace-lib-knex');

class Client  {

	constructor() {
		this.model = {};
	}

	setName(name) {
		this.model.name = name;
		return this;
	}

	setDoj(doj) {
		this.model.doj = doj;
		return this;
	}

	setExpiry(expiry) {
		this.model.expiry = expiry;
		return this;
	}

	setTenantId(tenantId) {
		this.model.tenant_id = tenantId;
		return this;
	}

	getDisplayable() {
		return ['id', 'name', 'doj', 'expiry'];
	}

	getFillable() {
		return ['name', 'doj', 'expiry', 'tenant_id'];
	}

	async all() {
		let fields = this.getDisplayable();
		return knex
			.select(...fields)
			.from('clients_client')
	}

	async info(clientId) {
		let fields = this.getDisplayable();
		return knex
			.select(...fields)
			.from('clients_client')
			.where('id', clientId);
	}

	async getMeta(tenantId) {

		return knex
			.select('id', 'name')
			.from('clients_client')
			.where('tenant_id', tenantId);
	}

	async findById(id) {
		try {
			let fields = this.getDisplayable();
			let client = await knex
					.select(...fields)
					.from('clients_client')
					.where('id', id);
			return client;				
		} catch(error) {
			return Promise.reject(error);
		}
	}

	create() {
		let columns = this.getFillable();
		return knex('clients_client')
			.insert(_.pick(this.model, columns));
	}

	update(id) {
		let columns = ['id', 'name', 'doj', 'expiry'];
		return knex('clients_client')
			.where('id', id)
			.update(_.pick(this.model, columns));
	}

	remove(id) {
		return knex('clients_client')
			.where('id', id)
			.del()
	}
}

module.exports = Client;