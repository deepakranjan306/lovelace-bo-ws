'use strict';

var ClientEntity = require('../entity/client');

class Client {

	async getAll() {
		try{
			let clientEntity = new ClientEntity();
			let client = await clientEntity.all();
			return client;
		} catch(error) {
			return Promise.reject(error);
		}
	}

	async getClientInfo(clientId) {
		try {
			let clientEntity = new ClientEntity();
			let client = await clientEntity.info(clientId);
			return client;
		} catch(error) {
			return Promise.reject(error);
		}
	}

	async getMeta(tenantId) {
		try {
			let clientEntity = new ClientEntity();
			let clients = await clientEntity.getMeta(tenantId);
			return clients;
		} catch(error) {
			return Promise.reject(error);
		}
	}

	async create(client) {
		try {
			let clientEntity = new ClientEntity();

			clientEntity
				.setName(client.name)
				.setDoj(client.doj)
				.setExpiry(client.expiry)
				.setTenantId(client.tenant_id);

				let createdClient = await clientEntity.create();

				let clientId = createdClient[0];
				return await clientEntity.findById(clientId);
		} catch(error) {
			return Promise.reject(error);
		}
	}

	async update(client) {
		try {
			let clientEntity = new ClientEntity();

			clientEntity
				.setName(client.name)
				.setDoj(client.doj)
				.setExpiry(client.expiry);

			await clientEntity.update(client.id, client.tenant_id);	
				
			return await clientEntity.findById(client.id);
		} catch(error) {
			return Promise.reject(error);
		}
	}

	async remove(clientId) {
		try {
			let clientEntity = new ClientEntity();

			await clientEntity.findById(clientId);
			await clientEntity.remove(clientId);
			return;
		} catch(error) {
			return Promise.reject(error);
		}
	}
}

module.exports = Client;