'use strict';

var ClientManager = require('../manager/client');
var _ = require('lodash');

class ClientController {

	static getAll(req, res) {
		let clientManager = new ClientManager();

		clientManager
			.getAll()
			.then((clients) => {
				return res.send(clients);
			})
			.catch((error) => {
				return res.send(error);
			});
	}

	static getMeta(req, res) {
		let clientManager = new ClientManager();

		clientManager
			.getMeta(1)
			.then((client) => {
				return res.send(client)
			})	
			.catch((error) => {
				return res.send(error)
			});
	}

	static getClientInfo(req, res) {
		let clientManager = new ClientManager();
		let clientId = Number(req.params.id);

		clientManager
			.getClientInfo(clientId)
			.then((client) => {
				return res.send(client);
			})
			.catch((error) => {
				return res.send(error);
			})
	}

	static create(req, res) {
		let keys = ['name', 'doj', 'expiry'];

		let data = _.extend({}, _.pick(req.body, keys), {
			tenant_id: 1
		});

		let clientManager = new ClientManager();

		clientManager
			.create(data)
			.then((client) => {
				return res.send(client);
			})
			.catch((error) => {
				return res.send(error);
			})
	}

	static update(req, res) {
		let keys = ['name', 'doj', 'expiry'];

		let data = _.extend({}, _.pick(req.body, keys), {
			id: Number(req.params.id)
		});

		let clientManager = new ClientManager();

		clientManager
			.update(data)
			.then((client) => {
				return res.send(client);
			})
			.catch((error) => {
				return res.send(error);
			})
	}

	static remove(req, res) {
		let clientManager = new ClientManager();
		let clientId = Number(req.params.id);

		clientManager
			.remove(clientId)
			.then(() => {
				return res.send("Deleted Id:" + req.params.id);
			})
			.catch((error) => {
				return res.send(error);
			})
	}
}

module.exports = ClientController;